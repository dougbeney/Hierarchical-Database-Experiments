import psycopg2
from pprint import pprint

# Source Material:
# https://www.sitepoint.com/hierarchical-data-database/
# Downsides of this method
# - slow for large datasets
# Upsides of this method
# - Easy to understand and implement

conn = psycopg2.connect("dbname=sandbox user=doug")

database_calls = 0

def sql_get(query):
	global database_calls
	cur = conn.cursor()
	cur.execute(query)
	database_calls += 1
	array = cur.fetchall()
	if len(array) == 1:
		return array[0]
	elif len(array == 0):
		return None
	return array

def display_children(parent, level):
	result = sql_get("SELECT title from foodtable where parent='%s';" % parent)
	for row in result:
		print('-'*level + row[0])
		display_children(row[0], level+1)

def get_path(title):
	parent = sql_get(
		"SELECT parent from foodtable where title='%s'" % title
	)

	parent = parent[0]

	path = []
	if parent:
		path = [parent]
		path = path + get_path(parent)

	return path

pprint(get_path("Apples"))

print("This method used", database_calls, "database calls")

conn.close()

