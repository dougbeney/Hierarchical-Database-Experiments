class Node:
	def __init__(self, data):
		self.left = None
		self.right = None
		self.data = data

	def insert(self, newNodeData):
		if self.data:
			if newNodeData < self.data:
				if self.left is None:
					self.left = Node(newNodeData)
				else:
					self.left.insert(newNodeData)
			elif newNodeData > self.data:
				if self.right is None:
					self.right = Node(newNodeData)
				else:
					self.right.insert(newNodeData)
		else:
			self.data = data

	def PrintTree(self):
		if self.left:
			self.left.PrintTree()
		print(self.data)
		if self.right:
			self.right.PrintTree()

	# Left -> Root -> Right
	def inorderTraversal(self, root):
		res = []
		if root:
			res = self.inorderTraversal(root.left)
			res.append(root.data)
			res = res + self.inorderTraversal(root.right)
		return res

root = Node(27)
root.insert(14)
root.insert(1)
root.insert(10)
root.insert(9)
root.insert(143)
root.insert(3)

print(root.inorderTraversal(root))

