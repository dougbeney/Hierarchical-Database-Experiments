import psycopg2
from pprint import pprint

# Source Material:
# https://www.sitepoint.com/hierarchical-data-database/

conn = psycopg2.connect("dbname=sandbox user=doug")

database_calls = 0

def sql_get(query):
	global database_calls
	cur = conn.cursor()
	cur.execute(query)
	database_calls += 1
	return cur.fetchall()

def sql_set(query):
	global database_calls
	cur = conn.cursor()
	cur.execute(query)
	database_calls += 1

def display_tree():
	results = sql_get("select * from foodtable order by lft asc")
	thread = []
	for row in results:
		name = row[0]
		lft = row[1]
		rgt = row[2]
		if len(thread) > 0:
			while thread[len(thread)-1] < rgt:
				del thread[len(thread)-1]

		print("-"*len(thread), name)
		thread.append(rgt)

def rebuildTree(parent, left):
	right = left+1
	results = sql_get(
		"select title from foodtable where parent='%s'" % parent
	)
	for row in results:
		row_title = row[0]
		right = rebuildTree(row_title, right)

	str = "UPDATE foodtable set lft='%s', rgt='%s' where title='%s';" % (left, right, parent)
	print(str)
	sql_set(
		str
	)
	return right+1

display_tree()

#rebuildTree("Food", 1)
#conn.commit()






print("This method used", database_calls, "database calls")
conn.close()

